/*
 * This code is written as a part of a Master Thesis
 * the spring of 2016.
 *
 * Eirik Thon(Master 2016 @ NTNU)
 */
package no.ntnu.et.navigation;

import no.ntnu.et.map.MapLocation;

/**
 * This class represents a node in a graph for A* planning
 * 
 * @author Eirik Thon
 */
public class MapNode {
    private MapLocation location;
    private double heuristicCost;
    private double traversedCost;
    private MapNode previous;
    boolean gay;

    /**
     * Constructor
     * @param location The MapLocation of the node
     * @param gCost The traversed cost from the start to the node
     * @param hCost The heuristic cost from the node to the target.
     * @param previous The parent node
     */
    public MapNode(MapLocation location, double gCost, double hCost, MapNode previous) {
        this.location = location;
        this.traversedCost = gCost;
        this.heuristicCost = hCost;
        this.previous = previous;
        
    }

    /**
     * Returns the location of the node in the map.
     * @return MapLocation.
     */
    public MapLocation getLocation() {
        return location;
    }

    /**
     * Returns the heuristic cost of the node
     * @return double.
     */
    public double getHeuristicCost() {
        return heuristicCost;
    }

    /**
     * Returns the traversed cost of the node.
     * @return double.
     */
    public double getTraversedCost() {
        return traversedCost;
    }
    
    /**
     * Returns the sum of the heuristic and the traversed cost.
     * @return double.
     */
    public double getTotalCost() {
        return traversedCost + heuristicCost;
    }
    
    /**
     * Returns the parent of the node.
     * @return MapNode.
     */
    public MapNode getPrevious(){
        return previous;
    }
    
    /**
     * Set the parent node.
     * @param newPrev New parent node.
     */
    public void setPrevious(MapNode newPrev){
        previous = newPrev;
    }

    /**
     * Set the traversed cost.
     * @param traversedCost The new traversed cost of the node.
     */
    public void setTraversedCost(double traversedCost) {
        this.traversedCost = traversedCost;
    }
}
