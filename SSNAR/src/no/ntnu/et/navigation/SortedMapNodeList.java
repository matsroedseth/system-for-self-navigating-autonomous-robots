/*
 * This code is written as a part of a Master Thesis
 * the spring of 2016.
 *
 * Eirik Thon(Master 2016 @ NTNU)
 */
package no.ntnu.et.navigation;

import no.ntnu.et.map.MapLocation;
import java.util.ArrayList;

/**
 * A class for easily sorting map nodes based og cost.
 * 
 * @author Eirik Thon
 */
public class SortedMapNodeList extends ArrayList<MapNode>{
    
    /**
     * Adds a node to the ArrayList at a position that keeps the list sorted
     * on the total cost of the nodes
     * @param node The node that is added.
     */
    public void sortedAdd(MapNode node){
        double cost = node.getTotalCost();
        int index = 0;
        for(MapNode otherNode: this){
            double otherCost = otherNode.getTotalCost();
            if(otherCost > cost){
                break;
            }
            index++;
        }
        super.add(index, node);
    }
    
    /**
     * Checks if the list contains a node with the specified location.
     * @param location The location to check
     * @return true of the list contains a node with the specified location and
     * false otherwise.
     */
    public MapNode containsLocation(MapLocation location){
        for(MapNode node: this){
            if(MapLocation.equals(location, node.getLocation())){
                return node;
            }
        }
        return null;
    }
}
