/*
 * This code is written as a part of a Master Thesis
 * the spring of 2016.
 *
 * Eirik Thon(Master 2016 @ NTNU)
 */
package no.ntnu.et.navigation;


import java.util.ArrayList;
import no.ntnu.et.general.Position;

/**
 * This class contains data for each robot used for navigation. It has flags to
 * see if a robot is in collision management and stores the way points used
 * for path following.
 * 
 * @author Eirik Thon
 */

public class NavigationRobot {

    private ArrayList<Position> waypoints;

    private int[] priorityCommand;
    
    private boolean hasPriority;

    private Position lastWaypoint;
    
    private boolean inWallCollision;
    
    private boolean inRobotCollision;
    
    private boolean inCollisionManagement;
    
    /**
     * Constructor
     * @param currentPosition The current position of the real robot in the map
     */
    public NavigationRobot(Position currentPosition) {
        this.lastWaypoint = currentPosition;
        waypoints = new ArrayList<Position>();
        priorityCommand = new int[2];
    }
   
    /**
     * Adds new way points to the NavigationRobot
     * @param newWaypoints An ArrayList of positions representing the way points
     */
    public void addWaypoints(ArrayList<Position> newWaypoints) {
        waypoints.addAll(newWaypoints);
    }

    /**
     * Updates the current priority command
     * @param newCommand Integer array containing rotation and distance.
     */
    public void setPriorityCommand(int[] newCommand) {
        priorityCommand = newCommand;
        hasPriority = true;
    }

    /**
     * Returns and removes the first way point among the stored way points. 
     * Each call to this function will return a new way point.
     * @return 
     */
    Position getNextWaypoint() {
        if(waypoints.size() > 0){
            lastWaypoint = waypoints.remove(0);
            return lastWaypoint;
        }
        return null;
    }
    
    /**
     * Returns the current priority command if it has not been returned before.
     * @return the current priority command if it has not been returned before.
     */
    int[] getPriorityCommand() {
        if(hasPriority == true){
            hasPriority = false;
            return priorityCommand;
        }
        return null;
    }
    
    /**
     * Adds the last way point to the beginning of the list of way points.
     */
    void redoLastWaypoint(){
        waypoints.add(0, lastWaypoint);
    }
    
    /**
     * Check if the NavigationRobot has a priority command that has not been
     * seen before
     * @return true if a new command exists, false otherwise.
     */
    boolean hasNewPriorityCommand(){
        return hasPriority;
    }

    /**
     * get the last way point (not the final way point).
     * @return the last way point.
     */
    public Position getLastWaypoint() {
        return lastWaypoint;
    }
    
    /**
     * Deletes all way points.
     */
    void clearWaypoints(){
        waypoints.clear();
    }
    
    /**
     * Returns all the way points.
     * @return returns an array of positions containing the way points.
     */
    ArrayList<Position> getWaypoints(){
        return waypoints;
    }
    
    /**
     * Check if the NavigationRobot has stored more way points.
     * @return true if the NavigationRobot contains more way points.
     */
    boolean hasMoreWaypoints(){
        if(waypoints.size()> 0){
            return true;
        }
        return false;
    }
    
    /**
     * Check if the robot is in collision management.
     * @return true if the robot is in collision management and false otherwise.
     */
    boolean isInCollisionManagement(){
        return inCollisionManagement;
    }
    
    /**
     * Set collision management status.
     * @param bool The new collision management status.
     */
    void setInCollisionManagement(boolean bool){
        inCollisionManagement = bool;
    }
}
