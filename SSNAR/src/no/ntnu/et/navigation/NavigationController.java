/*
 * This code is written as a part of a Master Thesis
 * the spring of 2016.
 *
 * Eirik Thon(Master 2016 @ NTNU)
 */
package no.ntnu.et.navigation;

import java.util.ArrayList;
import java.util.HashMap;
import no.ntnu.et.general.Position;
import no.ntnu.et.map.GridMap;
import no.ntnu.tem.application.Application;
import no.ntnu.tem.application.RobotController;
import no.ntnu.tem.robot.Robot;

/**
 * This class checks for commands in all of the Navigation Robots and sends
 * them to the robots via the Application. If a robot has no new commands this
 * class creates a new worker thread in RobotTaskManager to find a new task for
 * the robot.
 * 
 * @author Eirik Thon
 */

public class NavigationController extends Thread{
    

    private CollisionManager collisionManager;

    private RobotTaskManager robotTaskManager;

    private RobotController robotController;

    private Application application;

    private HashMap<String, NavigationRobot> navigationRobots;

    private ArrayList<String> robotNames;
    
    private boolean paused;
    
    private boolean debug = false;

    
    /**
     * Constructor
     * @param robotController The robotControl object used for accessing the 
     * robot objects.
     * @param application The Application object used for sending commands to
     * robots
     * @param map The GridMap object used for navigation
     */
    public NavigationController(RobotController robotController, Application application, GridMap map) {
        this.robotController = robotController;
        this.application = application;
        robotTaskManager = new RobotTaskManager(map);
        collisionManager = new CollisionManager(map, robotController);
        collisionManager.setName("Collision management");
        robotNames = new ArrayList<String>();
        navigationRobots = new HashMap<String, NavigationRobot>();
    }
    
    /**
     * Adds a new robot to the navigation controller.
     * @param robotName The name of the new robot.
     * @param id Th ID of the new robot.
     */
    public void addRobot(String robotName, int id) {
        Position currentPosition = new Position(robotController.getRobot(robotName).getPosition());
        NavigationRobot newNavRobot = new NavigationRobot(currentPosition);
        navigationRobots.put(robotName, newNavRobot);
        robotNames.add(robotName);
        collisionManager.addRobot(robotName, newNavRobot);
    }
    
    /**
     * Removes a robot from the navigation controller.
     * @param robotName The name of the robot that should be removed.
     */
    public void removeRobot(String robotName) {
        robotNames.remove(robotName);
        collisionManager.removeRobot(robotName);
    }
    
    /**
     * Starts the navigation controller.
     */
    @Override
    public void start(){
        if(!isAlive()){
            super.start();
            collisionManager.start();
        }
        else{
            collisionManager.unpause();
        }
        resumeAllRobots();
        paused = false;
    }

    /**
     * Pauses the navigation controller
     */
    public void pause(){
        collisionManager.pause();
        stopAllRobots();
        paused = true;
    }
    
    /**
     * Tells all the robots to pause
     */
    private void stopAllRobots(){
        for(int i = 0; i < robotNames.size(); i++){
            String name = robotNames.get(i);
            Robot robot = robotController.getRobot(name);
            int id = robot.getId();
            application.pauseRobot(id);
        }
    }
    
    /**
     * Tells all the robots to resume operation
     */
    private void resumeAllRobots(){
        for(int i = 0; i < robotNames.size(); i++){
            String name = robotNames.get(i);
            Robot robot = robotController.getRobot(name);
            int id = robot.getId();
            application.unPauseRobot(id);
        }
    }
    
    /**
     * This function sends commands to the Application to make the robots move.
     * The navigation is tone ether using way points or priority commands. 
     * way points marks the way to the robots current target and is used for
     * exploration and moving around in the environment. Priority commands are
     * given by the collision manager to make the robots avoid colliding into
     * things. Both way points and priority commands are stored in objects
     * called NavigationRobots.
     */
    @Override
    public void run() {
        setName("Navigation Controller");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        boolean finished = false;
        while(!finished) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                stopAllRobots();
                break;
            }
            if(paused){
                continue;
            }
            
            for(int i = 0; i < robotNames.size(); i++){
                String name = robotNames.get(i);
                Robot applicationRobot = robotController.getRobot(name);
                int id = applicationRobot.getId();
                if(navigationRobots.get(name).hasNewPriorityCommand()){
                    int[] nextCommand = navigationRobots.get(name).getPriorityCommand();
                    if(debug){
                        System.out.println(name + ": Executing next command, ROTATION " + nextCommand[0] + ", DISTANCE "+ nextCommand[1]);
                    }
                    application.writeCommandToRobot(id, name, nextCommand[0], nextCommand[1]); 
                }
                
                else if(!applicationRobot.isBusy() && !navigationRobots.get(name).isInCollisionManagement()){
                    if(navigationRobots.get(name).hasMoreWaypoints()){
                        // Get next target
                        Position nextWaypoint = navigationRobots.get(name).getNextWaypoint();
                        // Get current obot location and orientation
                        Position currentPosition = new Position(applicationRobot.getPosition());
                        int currentOrientation = applicationRobot.getRobotOrientation();
                        // Command the robot to move to the next waypoint along its path
                        int[] newCommand = findCommandToTargetPoint(nextWaypoint, currentPosition, currentOrientation);
                        if(debug){
                            System.out.println(name+ ": Executing next command, ROTATION " + newCommand[0] + ", DISTANCE "+ newCommand[1]);
                        }
                        application.writeCommandToRobot(id, name, newCommand[0], newCommand[1]); 
                    }
                    else if(!robotTaskManager.isWorkingOnTask(name)){
                        if(debug){
                            System.out.println(name + ": Idle. Searching for best target");
                        }
                        robotTaskManager.createNewTask(robotController.getRobot(name), navigationRobots.get(name), name);
                    }
                }
            }
        }
    }
    
    
    /**
     * Finds a command for making a robot reach the specified target, based
     * ons the robots current position and heading.
     * @param target The target for the robot
     * @param currentPosition The robots current position
     * @param robotHeading the robots current heading
     * @return command for making a robot reach the specified target, based
     * ons the robots current position and heading.
     */
    static int[] findCommandToTargetPoint(Position target, Position currentPosition, int robotHeading){
        int distance = (int)Position.distanceBetween(currentPosition, target);
        int rotation = ((int)Position.angleBetween(currentPosition, target).getValue()-robotHeading+360)%360;
        if(rotation > 180){
            rotation -= 360;
        }
        int[] command = {rotation, distance};
        return command;
    }
}