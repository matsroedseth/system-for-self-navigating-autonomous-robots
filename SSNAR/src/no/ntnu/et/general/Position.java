/*
 * This code is written as a part of a Master Thesis
 * the spring of 2016.
 *
 * Eirik Thon(Master 2016 @ NTNU)
 */
package no.ntnu.et.general;
import java.awt.Graphics2D;

/**
 * This class represents a position using an x-value, an a y-value
 * 
 * @author Eirik Thon
 */
public class Position {
    
    private double xValue;
    
    private double yValue;
    
    /**
     * Constructor.
     * Sets the x and y values of the position to zero.
     */
    public Position() {
        xValue = 0;
        yValue = 0;
    }

    /**
     * Constructor.
     * Creates a new Position object specified by the input parameters.
     * @param xPosition The initial x-value of the position in cm.
     * @param yPosition The initial y-value of the position in cm.
     */
    public Position(double xPosition, double yPosition) {
        this.xValue = xPosition;
        this.yValue = yPosition;
    }
    
    /**
     * Constructor.
     * Creates a new Position object.
     * @param position An array containing the initial x- and y-values of the
     * position.
     */
    public Position(int[] position) {
        this.xValue = position[0];
        this.yValue = position[1];
    }
    
    /**
     * Returns the x-value of the Position object.
     * @return the x-value of the position.
     */
    public double getXValue() {
        return xValue;
    }

    /**
     * Returns the x-value of the Position object.
     * @return the y-value of the position.
     */
    public double getYValue() {
        return yValue;
    }
    
    /**
     * Creates a deep copy of the specified Position object.
     * @param position The Position that should be copied.
     * @return A deep copy of the position object.
     */
    public static Position copy(Position position) {
        return new Position(position.xValue, position.yValue);
    }
 
    /**
     * prints the x-, y-values of the Position object.
     */
    public void print() {
        System.out.println("x-position " + xValue + ", y-position " + yValue);
    }
    
    /**
     * Draws a circle centered at the position onto a Graphics2D object.
     * @param g2D The Graphics2D object that the circle is painted onto.
     * @param diameter The diameter of the circle.
     * @param scale A scaling factor used for making the circle bigger and
     * smaller.
     */
    public void drawCircle(Graphics2D g2D, int diameter, double scale) {
        g2D.drawOval((int)((Math.round(xValue)-diameter/2)*scale),(int)((Math.round(yValue)-diameter/2)*scale), (int)((double)diameter*scale), (int)((double)diameter*scale));
    }
    
    /**
     * Draws an X centered at the position onto a Graphics2D object. Size
     * determines length of the arms and legs of the X.
     * @param g2D The Graphics2D object that the X is painted onto.
     * @param size The distance from the center of the X to its endpoints.
     * @param scale A scaling factor used for making the cross bigger and
     * smaller.
     */
    public void drawX(Graphics2D g2D, int size, double scale) {
        g2D.drawLine((int)(Math.round(xValue-size)*scale), (int)(Math.round(yValue-size)*scale),
                                (int)(Math.round(xValue+size)*scale), (int)(Math.round(yValue+size)*scale));
        g2D.drawLine((int)(Math.round(xValue-size)*scale), (int)(Math.round(yValue+size)*scale),
                (int)(Math.round(xValue+size)*scale), (int)(Math.round(yValue-size)*scale));
    }
    
    /**
     * Adds the x-,y-values of some other Position object to the values of the
     * Position object.
     * @param other The Position that is added.
     */
    public void add(Position other) {
        xValue += other.xValue;
        yValue += other.yValue;
    }
    
    /**
     * Creates a new position that is the sum of two positions.
     * @param pos1 The first position in the sum.
     * @param pos2 The second position in the sum.
     * @return the sum of the two positions in a new Position object.
     */
    public static Position sum(Position pos1, Position pos2) {
        Position sum = new Position(0, 0);
        sum.xValue = pos1.xValue + pos2.xValue;
        sum.yValue = pos1.yValue + pos2.yValue;
        return sum;
    }
    
    /**
     * Returns the distance between two positions.
     * @param pos1 The first position.
     * @param pos2 The second position.
     * @return the distance between the first and the second position.
     */
    public static double distanceBetween(Position pos1, Position pos2){
        return Math.sqrt(Math.pow(pos1.xValue - pos2.xValue, 2) + Math.pow(pos1.yValue - pos2.yValue, 2));
    }
    
    /**
     * Finds the angle of a line that goes through two positions, and returns
     * it as a new Angle object.
     * @param pos1 The first position.
     * @param pos2 The second position.
     * @return the angle between of a line through the two positions.
     */
    public static Angle angleBetween(Position pos1, Position pos2) {
        return new Angle(Math.toDegrees(Math.atan2(pos2.yValue - pos1.yValue, pos2.xValue - pos1.xValue)));
    }
    
    /**
     * Applies a rotational matrix to the position rotating it the specified
     * angle about origo.
     * @param rotation The angle of the rotation.
     */
    public void transform(Angle rotation){
        double rad = Math.toRadians(rotation.getValue());
        double xTemp = xValue;
        double yTemp = yValue;
        xValue = Math.cos(rad)*xTemp+-Math.sin(rad)*yTemp;
        yValue = Math.sin(rad)*xTemp+Math.cos(rad)*yTemp;
    }
}
