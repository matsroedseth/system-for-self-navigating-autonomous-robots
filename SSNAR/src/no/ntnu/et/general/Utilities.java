/*
 * This code is written as a part of a Master Thesis
 * the spring of 2016.
 *
 * Eirik Thon(Master 2016 @ NTNU)
 */
package no.ntnu.et.general;


import java.awt.Color;

/**
 * This class contains methods useful to other classes.
 * 
 * @author Eirik Thon
 */
public class Utilities {
    
    /**
     * Computes the Cartesian coordinates of the input parameters and returns
     * them as a new Position object.
     * @param theta The polar angle.
     * @param r The polar distance.
     * @return a new Position object from polar coordinates.
     */
    public static Position polar2cart(Angle theta, double r){
        double thetaRad = Math.toRadians(theta.getValue());
        double x = r*Math.cos(thetaRad);
        double y = r*Math.sin(thetaRad);
        Position cart = new Position(x,y);
        return cart;
    }
    
    /**
     * Creates a new Position from data in a string. The input string must be
     * on the format "x,y" or the function will fail.
     * @param string The input string containing the x and y values.
     * @return a new Position from the values in the string.
     */
    public static Position string2Position(String string){
        String[] stringParts = string.split(",");
        double x = Integer.parseInt(stringParts[0]);
        double y = Integer.parseInt(stringParts[1]);
        Position position = new Position(x,y);
        return position;
    }

    /**
     * Selects a color within a predefined set of colors. The parameter "i" is
     * used to select a color from the set. Modulo is used if i is larger than
     * the size of the set
     * @param i integer specifying the color.
     * @return a color specified by the integer.
     */
    public static Color selectColor(int i) {
        switch (i % 10) {
            case 0:
                return Color.blue;
            case 1:
                return Color.red;
            case 2:
                return Color.green;
            case 3:
                return Color.magenta;
            case 4:
                return Color.orange;
            case 5:
                return Color.cyan;
            case 6:
                return Color.pink;
            case 7:  
                return Color.lightGray;
            case 8:
                return Color.darkGray;
            case 9:
                return Color.yellow;
            default:
                return Color.black;
        }
    }
    
    /**
     * Finds the intersection points of  Line object and a circle. The method
     * returns the distance to the nearest intersection point along the line.
     * If no intersection exists the method returns 0. 
     * @param line A line object.
     * @param circleCenter A position object that specifies the center of the
     * circle.
     * @param circleRadius The radius of the circle.
     * @return The function returns the distance from the start-position of the
     * line to the nearest intersection with the circle. If there are no
     * intersection, it returns null.
     */
    public static double lineCircleIntersection(Line line, Position circleCenter, double circleRadius) {
        double v_x = line.getDirection()[0];
        double v_y = line.getDirection()[1];
        double p_x = line.getStart()[0];
        double p_y = line.getStart()[1];
        double lineLength = line.getLength();
        double x_c = circleCenter.getXValue();
        double y_c = circleCenter.getYValue();
        double r = circleRadius;
        // See Thon 2016 on where this monstrous equation comes from
        double t1 = (Math.sqrt(-Math.pow(p_x*v_y,2) + 2*p_x*p_y*v_x*v_y - 2*p_x*v_x*v_y*y_c + 2*p_x*Math.pow(v_y,2)*x_c - Math.pow(p_y*v_x,2) + 2*p_y*Math.pow(v_x,2)*y_c - 2*p_y*v_x*v_y*x_c + Math.pow(r*v_x,2) + Math.pow(r*v_y,2) - Math.pow(v_x*y_c,2) + 2*v_x*v_y*x_c*y_c - Math.pow(v_y*x_c,2)) - p_x*v_x - p_y*v_y + v_x*x_c + v_y*y_c)/(Math.pow(v_x,2) + Math.pow(v_y,2));
        double t2 = -(Math.sqrt(-Math.pow(p_x*v_y,2) + 2*p_x*p_y*v_x*v_y - 2*p_x*v_x*v_y*y_c + 2*p_x*Math.pow(v_y,2)*x_c - Math.pow(p_y*v_x,2) + 2*p_y*Math.pow(v_x,2)*y_c - 2*p_y*v_x*v_y*x_c + Math.pow(r*v_x,2) + Math.pow(r*v_y,2) - Math.pow(v_x*y_c,2) + 2*v_x*v_y*x_c*y_c - Math.pow(v_y*x_c,2)) + p_x*v_x + p_y*v_y - v_x*x_c - v_y*y_c)/(Math.pow(v_x,2) + Math.pow(v_y,2));
        if(t1 != Double.NaN && t2 != Double.NaN){
            if(t1 < t2 && t1 > 0 && t1 < lineLength){
                return t1;
            }
            if(t2 < t1 && t2 > 0 && t2 < lineLength){
                return t2;
            }
        }
        return 0;
    }
    
    
    /**
     * Finds the intersection point between two lines. If no intersection exists
     * the method returns 0.
     * @param line1 The first Line object.
     * @param line2 The second Line object.
     * @return the distance from the start-position of the first line to the
     * intersection or zero if there are no intersection.
     */
    public static double lineLineIntersection(Line line1, Line line2){//double[] line1Start, double[] line1Vector, double line1Length, double[] line2Start, double[] line2Vector, double line2Length) {
        double s1_x = line1.getStart()[0];
        double s1_y = line1.getStart()[1];
        double s2_x = line2.getStart()[0];
        double s2_y = line2.getStart()[1];
        double v1_x = line1.getDirection()[0];
        double v1_y = line1.getDirection()[1];
        double v2_x = line2.getDirection()[0];
        double v2_y = line2.getDirection()[1];
        double l1 = line1.getLength();
        double l2 = line2.getLength();
        
        double div = v2_y*v1_x - v2_x*v1_y;
        if (div == 0) {
            return 0;
        }
        double t = ((s2_x-s1_x)*v2_y+(s1_y-s2_y)*v2_x)/div;
        double u = ((s1_y-s2_y)*v1_x+(s2_x-s1_x)*v1_y)/div;
        
        if (t > 0 && u > 0 && t < l1 && u < l2) {
            return t;
        }
        else
            return 0;
    }
    
    /**
     * The method projects the input position onto the Line object and returns
     * the projected Position.
     * @param point The position that should be projected onto the line
     * @param line A line object
     * @return the orthogonal projection of the position onto the line
     */
    public static Position getOrthogonalProjection(Position point, Line line){
        double xDiff = point.getXValue() - line.getStart()[0];
        double yDiff = point.getYValue() - line.getStart()[1];

        // Create unitvector from feature
        double[] dir = line.getDirection();

        // Project a onto b
        double projectionLength = xDiff*dir[0] + yDiff*dir[1];

        return line.findPositionAlongLine(projectionLength);
    }
}
