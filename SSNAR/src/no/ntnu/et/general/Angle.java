/*
 * This code is written as a part of a Master Thesis
 * the spring of 2016.
 *
 * Eirik Thon(Master 2016 @ NTNU)
 */
package no.ntnu.et.general;

import java.awt.Graphics2D;

/**
 * This class is used to represent angles. The angle is represented as a double
 * in the range [0,360). All functions in this class are implemented so that the 
 * value stays within these bounds.
 * 
 * @author Eirik Thon
 */
public class Angle {
    
    private double value;

    /**
     * Constructs a new Angle with value 0.
     */
    public Angle() {
        value = 0;
    }
    
    /**
     * Constructs a new Angle with value specified by the input parameter.
     * @param value The value of the angle.
     */
    public Angle(double value) {
        this.value = value % 360;
        if (value < 0) {
            value += 360;
        }
    }
    
    /**
     * Returns the value of the angle.
     * @return the value of Angle.
     */
    public double getValue() {
        return value;
    }
    
    /**
     * Creates a deep copy of the input Angle.
     * @param angle The Angle that will be copied.
     * @return a new Angle object equal to the input parameter.
     */
    public static Angle copy(Angle angle) {
        return new Angle(angle.value);
    }
    
    /**
     * Adds the input Angle to the Angle. The The angle is kept between 0 and
     * 360 degrees.
     * @param addedAngle The Angle that should be added.
     */
    public void add(Angle addedAngle){
        value = (value + addedAngle.value) % 360;
    }
    
    /**
     * Adds the input value to the Angle. The The angle is kept between 0 and
     * 360 degrees.
     * @param addedAngle The value that should be added.
     */
    public void add(double addedAngle){
        value = (value + (addedAngle % 360)) % 360;
        if(value < 0) {
            value += 360;
        }
    } 
    
    /**
     * Adds two angles together and returns the result as a new Angle object. 
     * The value of the resulting angle is kept between 0 and 360 degrees.
     * @param angl1 The first Angle in the sum.
     * @param angl2 The second Angle in the sum.
     * @return the sum of the two angles in an new Angle object.
     */
    public static Angle sum(Angle angl1, Angle angl2) {
        double result = (angl1.value + angl2.value)%360;
        return new Angle(result);
    }
    
    /**
     * Finds the lowest difference between two angles, and returns it as a new
     * Angle object.
     * @param angl1 The first angle.
     * @param angl2 The second angle.
     * @return the lowest difference between the two angles in a new Angle.
     * object
     */
    public static double difference(Angle angl1, Angle angl2) {
        double result = Math.abs(angl1.value - angl2.value);
        if(result <= 180){
            return result;
        }else{
            return 360 - result;
        }
    }
    
    /**
     * Prints the value of the angle.
     */
    public void print() {
        System.out.println("Angle: " + value);
    }
    
    /**
     * Draws a line onto a Graphics2D object starting in the specified position
     * along the angle. The starting position and length of the line is
     * specified in the parameters.
     * @param g2D The Graphics2D object the line is drawn onto.
     * @param xStart The x-value of the starting point of the line.
     * @param yStart The y-value of the starting point of the line.
     * @param length The length of the line.
     * @param scale A factor that can be used for increasing the length of the
     * line.
     */
    public void drawAngle (Graphics2D g2D, double xStart, double yStart, int length, double scale) {
        Position headingPoint = Utilities.polar2cart(this, length);
        g2D.drawLine((int)(Math.round(xStart)*scale), (int)(Math.round(yStart)*scale),
                (int)(Math.round(xStart+headingPoint.getXValue())*scale), (int)(Math.round(yStart+headingPoint.getYValue())*scale));
    }
}
