/*
 * This code is written as a part of a Master Thesis
 * the spring of 2016.
 *
 * Eirik Thon(Master 2016 @ NTNU)
 */
package no.ntnu.et.general;

import java.awt.Graphics2D;

/**
 * This class is used to represent the pose of a robot. The pose consists of
 * the position and orientation of the robot.
 * 
 * @author Eirik Thon
 */
public class Pose {
    
    private Position position;
    
    private Angle heading;
    
    /**
     * Constructor
     * Creates a new pose object with all values set to 0.
     */
    public Pose(){
        position = new Position();
        heading = new Angle();
    }
    
    /**
     * Constructor
     * Creates a new pose object with values specified by the input parameters.
     * @param initXPos The initial x-position of the pose in cm from origo.
     * @param initYPos The initial y-position of the pose in cm from origo.
     * @param initOrientation The initial orientation of the pose in degrees.
     */
    public Pose(int initXPos, int initYPos, int initOrientation){
        position = new Position(initXPos, initYPos);
        heading = new Angle(initOrientation);
    }
    
    /**
     * Constructor
     * Creates a new pose object with values specified by the input parameters.
     * @param initialPosition The initial position of the pose.
     * @param initialHeading The initial orientation of the pose.
     */
    public Pose(Position initialPosition, Angle initialHeading){
        position = initialPosition;
        heading = initialHeading;
    }

    /**
     * Returns the position of the Pose object.
     * @return The position of the pose.
     */
    public Position getPosition() {
        return position;
    }

    /**
     * Returns the orientation of the Pose object.
     * @return The orientation of the pose.
     */
    public Angle getHeading() {
        return heading;
    }
    
    /**
     * Creates a deep copy of the specified pose.
     * @param pose The pose that will be copied.
     * @return a deep copy of the input pose.
     */
    public static Pose copy(Pose pose){
        return new Pose(Position.copy(pose.position), Angle.copy(pose.heading));
    }
    
    /**
     * Paints the pose onto a Graphics2D object. The pose is painted as a circle
     * at the position and a line indicating the heading.
     * @param g2D Graphics2D object to paint upon.
     * @param scale A factor that can be used for increasing the size of the
     * the painted pose.
     */
    public void paint(Graphics2D g2D, double scale){
        position.drawCircle(g2D, 10, scale);
        heading.drawAngle(g2D, position.getXValue(), position.getYValue(), 7, scale);
    }
    
    /**
     * Adds the input Angle to the Angle of the Pose object
     * @param rotation 
     */
    public void rotate(Angle rotation) {
        heading.add(rotation);
    }
    
    /**
     * Moves the position of the pose object forward in the current direction of
     * the pose.
     * @param distance The amount the pose is moved specified in cm.
     */
    public void move(double distance) {
        Position offset = Utilities.polar2cart(heading, distance);
        position.add(offset);
    }
    
    /**
     * Adds the position and heading of the input pose to the position and 
     * heading of the Pose object
     * @param pose2 The pose that should be added.
     */
    public void add(Pose pose2) {
        position.add(pose2.position);
        heading.add(pose2.heading);
    }
}
