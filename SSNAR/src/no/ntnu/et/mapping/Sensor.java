/*
 * This code is written as a part of a Master Thesis
 * the spring of 2016.
 *
 * Eirik Thon(Master 2016 @ NTNU)
 */
package no.ntnu.et.mapping;

import no.ntnu.et.general.Position;

/**
 * This class is used to store the position of an IR-measurement, and whether or
 * not it is an open measurement (meaning that the maximum distance of the 
 * sensors has been exceeded) or a measurement of an obstacle.
 * 
 * @author Eirik Thon
 */
public class Sensor {
    Position position;
    boolean measurement;

    /**
     * Constructor.
     * Default constructor.
     */
    public Sensor() {
    }
    
    /**
     * Constructor (NOT USED)
     * @param position Initial position of the measurement.
     * @param measurement Initial value of the measurement (free or occupied).
     */
    public Sensor(Position position, boolean measurement) {
        this.position = position;
        this.measurement = measurement;
    }

    /**
     * Returns the position of th measurement.
     * @return the position of th measurement.
     */
    public Position getPosition() {
        return position;
    }

    /**
     * Returns the value of the measurement (free or occupied)
     * @return true if occupied and false if free.
     */
    public boolean isMeasurement() {
        return measurement;
    }
    
    /**
     * Set the position of the measurement
     * @param position A Position object
     */
    public void setPosition(Position position) {
        this.position = position;
    }

    /**
     * Set the value of the measurement 
     * @param measurement The new value for the measurement (True if occupied
     * and false if free)
     */
    public void setMeasurement(boolean measurement) {
        this.measurement = measurement;
    }
}