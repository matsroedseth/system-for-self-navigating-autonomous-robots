/*
 * This code is written as a part of a Master Thesis
 * the spring of 2016.
 *
 * Eirik Thon(Master 2016 @ NTNU)
 */
package no.ntnu.et.mapping;

import no.ntnu.et.general.Angle;
import no.ntnu.et.general.Position;
import no.ntnu.et.general.Pose;
import no.ntnu.tem.robot.Measurement;
import no.ntnu.tem.robot.Robot;

/**
 * This class is used to find the location of the robot and each IR-measurement
 * in the map. 
 * 
 * @author Eirik Thon
 */

public class MeasurementHandler {
    final private  Pose initialPose;
    private int sensorRange;
    private Measurement currentMeasurement;
    private Pose robotPose;
    private Sensor[] sensors;
    private Robot robot;

    /**
     * Constructor.
     * Creates a new MeasurementHandler object.
     * @param robot The Robot object where the measurements are stored.
     * @param initialPose The initial pose of the related robot relative to the
     * map
     */
    public MeasurementHandler(Robot robot, Pose initialPose) {
        this.initialPose = initialPose;
        this.robot = robot;
        sensorRange = 40;
        sensors = new Sensor[4];
        for (int i = 0; i < 4; i++) {
            sensors[i] = new Sensor();
        }
    }
    
    /**
     * Computes and stores the position of all the measurements and the robot
     * relative to the map.
     * @return true if there was a new measurement stored in the Robot object
     * and false otherwise.
     */
    boolean updateMeasurement(){
        currentMeasurement = robot.getMeasurement();
        if(currentMeasurement== null){
            return false;
        }
        Position robotPosition = new Position((double) currentMeasurement.getxPos(), (double) currentMeasurement.getyPos());
        Angle robotAngle = new Angle(currentMeasurement.getTheta());
        robotPose = new Pose(robotPosition, robotAngle);
        robotPose.getPosition().transform(initialPose.getHeading());
        robotPose.add(this.initialPose);
        
        // Update sensor data
        int[] irData = currentMeasurement.getIRdata();
        int[] irheading = currentMeasurement.getIRHeading();
        for (int i = 0; i < 4; i++) {
            int measurementDistance = irData[i];
            if (measurementDistance == 0 || measurementDistance > sensorRange) {
                sensors[i].setMeasurement(false);
                measurementDistance = sensorRange;
            }else{
                sensors[i].setMeasurement(true);
            }
            Angle towerAngle  = new Angle((double)irheading[i]);
            Angle sensorAngle = Angle.sum(towerAngle, robotPose.getHeading());
            double xOffset = measurementDistance * Math.cos(Math.toRadians(sensorAngle.getValue()));
            double yOffset = measurementDistance * Math.sin(Math.toRadians(sensorAngle.getValue()));
            Position measurementPosition = Position.sum(robotPose.getPosition(), new Position(xOffset, yOffset));
            sensors[i].setPosition(measurementPosition);
        }
        return true;
    }
    
    /**
     * Returns the position of the robot.
     * @return the position of the robot.
     */
    Position getRobotPosition(){
        return robotPose.getPosition();
    }
    
    /**
     * Returns the heading of the robot.
     * @return the heading of the robot.
     */
    Angle getRobotHeading(){
        return robotPose.getHeading();
    }
    
    /**
     * Returns all the measurement data.
     * @return an array of Sensor objects containing the locations of the 
     * measurements.
     */
    Sensor[] getIRSensorData(){
        return sensors;
    }
}
