/*
 * This code is written as a part of a Master Thesis
 * the spring of 2016.
 *
 * Eirik Thon(Master 2016 @ NTNU)
 */
package no.ntnu.et.map;

import java.util.ArrayList;

/**
 * This class is used to represent a cell in a grid map.
 * 
 * The variables and functions concerning path and target were only used during
 * testing and are not important for the functionality of the system.
 * 
 * @author Eirik Thon
 */
public class Cell {
    private boolean previouslyObserved;
    private boolean restricted;
    private boolean weaklyRestricted;
    private boolean occupied;
    private boolean stateChanged;
    private boolean isTarget;
    private boolean isPath;
    
    ArrayList<Cell> restrictingCells; // Other cells that restricts this cell
    ArrayList<Cell> weaklyRestrictingCells; // Other cells that weakly restricts this cell
    
    
    /**
     * Constructor.
     * Creates a new cell with default initial values.
     */
    public Cell(){
        occupied = false;
        previouslyObserved = false;
        restricted = false;
        stateChanged = false;
        weaklyRestricted = false;
        isPath = false;
        isTarget = false;
        restrictingCells = new ArrayList<Cell>();
        weaklyRestrictingCells = new ArrayList<Cell>();
    }
    
    /**
     * Adds a cell to the restricting cell. If the cell has more than
     * one restricting cell its restricted status is set to true.
     * @param otherCell The restricting cell.
     */
    public void addRestrictingCell(Cell otherCell){
        restrictingCells.add(otherCell);
        if(!restrictingCells.isEmpty()){
            restricted = true;
        }
    }
    
    /**
     * Adds a cell to the weakly restricting cells. If the cell has more than
     * one weakly restricting cell its weakly restricted status is set to true.
     * @param otherCell The weakly restricting cell.
     */
    public void addWeaklyRestrictingCell(Cell otherCell){
        weaklyRestrictingCells.add(otherCell);
        if(!weaklyRestrictingCells.isEmpty()){
            weaklyRestricted = true;
        }
    }
    
    /**
     * Removes a cell from the restricting cells. If the cell has 0 restricting
     * cells its restricted status is set to false.
     * @param otherCell The cell that should be removed from restricting cells.
     */
    public void removeRestrictingCell(Cell otherCell){
        restrictingCells.remove(otherCell);
        if(restrictingCells.isEmpty()){
            restricted = false;
        }
    }
    
    
    
    /**
     * Removes a cell from the weakly restricting cells. If the cell has 0
     * weakly restricting cell its weakly restricted status is set to false.
     * @param otherCell The cell that should be removed from weakly restricting
     * cells.
     */
    public void removeWeaklyRestrictingCell(Cell otherCell){
        weaklyRestrictingCells.remove(otherCell);
        if(weaklyRestrictingCells.isEmpty()){
            weaklyRestricted = false;
        }
    }
    
    /**
     * Returns the restricted status of the Cell object.
     * @return true if the cell is restricted, and false otherwise.
     */
    public boolean isRestricted(){
        return restricted;
    }
    
    /**
     * Returns the weakly restricted status of the Cell object.
     * @return true if the cell is weakly restricted, and false otherwise.
     */
    public boolean isWeaklyRestricted(){
        return weaklyRestricted;
    }
    
    /**
     * Returns whether or not the cell has been set to a path.
     * @return true if the cell has been set to path, and false otherwise.
     */
    public boolean isPath(){
        return isPath;
    }
    
    /**
     * Sets the path flag of the cell to the specified value.
     * @param isPath The new value for the path-flag
     */
    public void setPath(boolean isPath){
        this.isPath = isPath;
    }
    
    /**
     * Returns whether or not the cell has been set to a target.
     * @return true if the cell has been set to target, and false otherwise.
     */
    public boolean isTarget(){
        return isTarget;
    }
    
    /**
     * Sets the target flag of the cell to the specified value.
     * @param isTarget The new value for the target-flag
     */
    public void setTarget(boolean isTarget){
        this.isTarget = isTarget;
    }
    
    /**
     * Updates the occupied status of the cell. If the cell has not been
     * observed before its "previously observed" status is set true. This the
     * "occupied status" of the cell changes the "changed state" status is set
     * true, and false otherwise.
     * @param measurement The new value of the "occupied status" 
     */
    void observe(boolean measurement) {          
        if (measurement){
            if(occupied == false){
                stateChanged = true;
            }
            else{
                stateChanged = false;
            }
            occupied = true;
        }
        else{
            if(occupied == true){
                stateChanged = true;
            }
            else{
                stateChanged = false;
            }
            occupied = false;
        }
        if(!previouslyObserved){
            previouslyObserved = true;
            stateChanged = true;
        }  
    }

    /**
     * Returns true if and only if the "occupied" variable changed during the 
     * latest measurement of the cell
     * @return true if the occupied status was changed, and false otherwise
     */
    boolean stateChanged(){
        return stateChanged;
    }
    
    /**
     * Returns the "previously observed" status of the cell. This status only
     * changes from false to true the first time the observe-function is called
     * @return true if the cell has been previously observed, and false
     * otherwise
     */
    public boolean isPreviouslyObserved() {
        return previouslyObserved;
    }
    
    /**
     * Returns true if the cell has been previously observed and is not occupied
     * @return boolean
     */
    public boolean isFree(){
        if (!previouslyObserved) {
            return false;
        }
        return !occupied;
    }
    
    /**
     * Returns true if the cell has been previously observed and is occupied
     * @return boolean
     */
    public boolean isOccupied(){
        if (!previouslyObserved) {
            return false;
        }
        return occupied;
    }
    
    /**
     * Returns true if the cell is free and not restricted
     * @return boolean
     */
    public boolean isWeaklyTargetable(){
        return (!occupied && previouslyObserved && !restricted);
    }
    
    /**
    * Returns true if the cell is free and not weakly restricted
    * @return boolean
    */
    public boolean isFreelyTargetable(){
        return (!occupied && previouslyObserved && !restricted && !weaklyRestricted);
    }
}
