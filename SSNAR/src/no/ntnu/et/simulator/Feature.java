/*
 * This code is written as a part of a Master Thesis
 * the spring of 2016.
 *
 * Eirik Thon(Master 2016 @ NTNU)
 */
package no.ntnu.et.simulator;

import no.ntnu.et.general.Position;
import java.awt.Graphics2D;


/**
 * This class represent a wall or obstacle in the map. The feature must be a
 * straight line, and it is parameterized by a start and end position.
 * 
 * @author Eirik Thon
 */
public class Feature {
    private Position start;
    private Position end;
    
    /**
     * Constructor
     * @param start The start position of the feature
     * @param end The end position of the feature
     */
    public Feature(Position start, Position end) {
        this.start = start;
        this.end = end;
    }
    
    /**
     * Constructor
     * @param xStart Start position x-value.
     * @param yStart Start position y-value.
     * @param xEnd End position x-value.
     * @param yEnd End position y-value.
     */
    public Feature(double xStart, double yStart, double xEnd, double yEnd) {
        this.start = new Position(xStart, yStart);
        this.end = new Position(xEnd, yEnd);
    }
    
    /**
     * Creates a deep copy of the specified feature.
     * @param feature The feature that is copied.
     * @return a copy of the input feature.
     */
    static Feature copy(Feature feature){
        return new Feature(Position.copy(feature.start), Position.copy(feature.end));
    }
    
    /**
     * Returns the start position of the feature.
     * @return start Position.
     */
    public Position getStartPosition() {
        return start;
    }

    /**
     * Returns the en position of the feature.
     * @return end Position.
     */
    public Position getEndPosition() {
        return end;
    }
   
    /**
     * Prints the position of the feature using System.out
     */
    void print(){
        start.print();
        end.print();
    }
    
    /**
     * Paints a line between the start and end position of the feature onto
     * "g2D".
     * @param g2D The Graphics2D object that the line is painted onto.
     */
    void paint(Graphics2D g2D, double scale) {
       // Draw line between start and end
       g2D.drawLine((int)(Math.round(start.getXValue())*scale), (int)(Math.round(start.getYValue())*scale),
                   (int)(Math.round(end.getXValue())*scale), (int)(Math.round(end.getYValue())*scale));        
    }
    
    /**
     * Returns the distance between the start and the end position.
     * @return double
     */
    public double getLength() {
        return Position.distanceBetween(start, end);
    }
}
