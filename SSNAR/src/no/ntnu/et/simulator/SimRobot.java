/*
 * This code is written as a part of a Master Thesis
 * the spring of 2016.
 *
 * Eirik Thon(Master 2016 @ NTNU)
 */
package no.ntnu.et.simulator;

import no.ntnu.et.general.Utilities;
import no.ntnu.et.general.Pose;
import no.ntnu.et.general.Angle;
import no.ntnu.et.general.Position;
import no.ntnu.et.general.Line;

/**
 * This class represents the virtual robots in the simulator. It has private
 * variables to represent the state of the robots such as the pose
 * The robots behavior is created by calling methods in this class such as
 * moveRobot().
 * 
 * @author Eirik Thon
 */
public class SimRobot {
    private SimWorld world;
    private Pose pose;
    private Pose estimatedPose;
    final private Pose initialPose;
    private String name;
    private int id;
    private double measuredDistance;
    private double measuredRotation;
    private double targetDistance;
    private double targetRotation;
    private int rotationDirection;
    private int movementDirection;
    private boolean rotationFinished;
    private boolean translationFinished;
    private Angle towerAngle;
    final private double turnSpeed = 0.5;
    final private double moveSpeed = 0.1;
    final private double towerSpeed = 0.25;
    private int towerDirection;
    final private Object movementLock = new Object();
    final private double maxVisualLength = 80;
    final private double minVisualLength = 10;
    private double[] lastIrMeasurement;
    private Position targetPosition;
    private int diameter = 10;
    
    /**
     * Constructor for Robot.
     * @param world The simulated environment that the robot operates within.
     * @param initialPose The initial pose of the robot in the simulator.
     * @param name The name of the robot.
     * @param id The ID of the robot.
     */
    public SimRobot(SimWorld world, Pose initialPose, String name, int id) {
        this.world = world;
        towerDirection = 1;
        towerAngle = new Angle(0);
        this.name = name;
        this.id = id;
        pose = initialPose;
        this.initialPose = Pose.copy(initialPose);
        estimatedPose = new Pose(0, 0, 0);
        targetDistance = 0;
        targetRotation = 0;
        measuredDistance = 0;
        measuredRotation = 0;
        lastIrMeasurement = new double[4];
        rotationDirection = 1;
        movementDirection = 1;
        rotationFinished = true;
        translationFinished = true;       
        targetPosition = Position.copy(pose.getPosition());
    }
    
    /**
     * Returns the max distance of the sensors.
     * @return double
     */
    double getMaxSensorDistance(){
        return maxVisualLength;
    }
    
    /**
     * Returns the current angle of the sensor tower.
     * @return double
     */
    Angle getTowerAngle() {
        return towerAngle;
    }
    
    /**
     * Returns the ID of the robot
     * @return integer
     */
    int getId() {
        return id;
    }
    
    /**
     * Returns the name of the robot
     * @return String
     */
    String getName() {
        return name;
    }
    
    /**
     * Sets the target rotation and target heading. The measured
     * rotation and distance are reset to 0.
     * @param theta Angle the robot should rotate.
     * @param distance The distance the robot should move.
     */
    void setTarget(double theta, double distance) {
        synchronized(movementLock) {
            targetRotation = theta;
            targetDistance = distance;
            measuredRotation = 0;
            measuredDistance = 0;
            rotationDirection = (int)Math.signum(theta);
            movementDirection = (int)Math.signum(distance);
            Angle targetAngle = Angle.sum(pose.getHeading(), new Angle(theta));
            Position offset = Utilities.polar2cart(targetAngle, distance);
            targetPosition = Position.sum(pose.getPosition(), offset);
            rotationFinished = false;
            translationFinished = false;
        }
    }
    
    /**
     * Returns the pose of the robot.
     * @return Pose
     */
    Pose getPose() {
        return pose;
    }
    
    /**
     * Returns the initial pose of the robot.
     * @return Pose
     */
    Pose getInitialPose() {
        return initialPose;
    }
    
    /**
     * Returns the estimated pose of the robot
     * @return Pose
     */
    Pose getEstimatedPose() {
        return estimatedPose;
    }
    
    /**
     * Returns the target position of the robot.
     * @return Position.
     */
    Position getTargetPosition(){
        return targetPosition;
    }
    
    /**
     * Turns the tower 5 degrees within a 90 degree angle.
     */
    void turnTower() {
        int clockWise = -1;
        int counterClockWise = 1;
        if (towerAngle.getValue() < towerSpeed && towerDirection == clockWise) {
            towerDirection = counterClockWise;
        } else if (towerAngle.getValue() >= 90 && towerDirection ==  counterClockWise) {
            towerDirection = clockWise;
        }
        towerAngle.add(towerDirection * towerSpeed);
    }

    /**
     * Moves the robot a little bit according to its current command. If the
     * robot has reached its current target the function does nothing. If the
     * robot has collided with another object the real pose does not change, but
     * the estimated pose is updated. If the robot has reach its current target
     * the function does nothing.
     * @param noise A value that is added to the movement of the estimated pose.
     */
    boolean moveRobot(double noise) {
        synchronized(movementLock) {
            if (!rotationFinished) {
                if (Math.abs(measuredRotation) >= Math.abs(targetRotation)){
                    measuredRotation = 0;
                    rotationFinished = true;
                }
                pose.rotate(new Angle(turnSpeed*rotationDirection));
                estimatedPose.rotate(new Angle((turnSpeed+noise)*rotationDirection));
                measuredRotation += (turnSpeed+noise)*rotationDirection;
            } else if (!translationFinished) {
                if(Math.abs(measuredDistance) >= Math.abs(targetDistance)) {
                    measuredDistance = 0;
                    translationFinished = true;
                    return true;
                }
                Pose testPose = Pose.copy(pose);
                testPose.move(moveSpeed*movementDirection);
                estimatedPose.move((moveSpeed+noise)*movementDirection);
                measuredDistance += (moveSpeed+noise)*movementDirection;
                if (world.checkIfPositionIsFree(testPose.getPosition(), id)) {
                    pose.move(moveSpeed*movementDirection);
                }
            }
            return false;
        }
    }
    
    /**
     * Computes and stores measurements of nearby obstacles. The value of 
     * sensorNoise is added to each measurement.
     * @param sensorNoise Measurement error.
     */
    void measureIR(double sensorNoise) {
        Position lineOfSightStart = pose.getPosition();
        Angle lineOfSightAngle = Angle.sum(towerAngle, pose.getHeading());
        for (int i = 0; i < 4; i++) {
            lastIrMeasurement[i] = 0;
            
            // Create a feature along the line of sight for each sensor
            if(i > 0){
                lineOfSightAngle.add(90);
            }
            Position offset = Utilities.polar2cart(lineOfSightAngle, maxVisualLength);
            Position lineOfSightEnd = Position.sum(lineOfSightStart, offset);
            Line lineOfSight = Line.convertFeatureToLine(new Feature(lineOfSightStart, lineOfSightEnd));
            lastIrMeasurement[i] = world.findNearestIntersection(lineOfSight, id);
            if(lastIrMeasurement[i] != 0){
                lastIrMeasurement[i] += sensorNoise;
            }
        }
    }
    
    /**
     * Generates a hand shake message
     * @return String
     */
    String generateHandshake() {
        int robotWidth = 5;
        int robotLength = 5;
        int toweroffset[] = {0, 0};
        int axleoffset = 0;
        int[] sensoroffset = {0, 0, 0, 0};
        int[] irheading = {0, 90, 180, 270};
        int messageDeadline = 400;
        return "{H," + robotWidth + "," + robotLength + "," + toweroffset[0] + "," + toweroffset[1] + "," + axleoffset + "," + sensoroffset[0] + "," + sensoroffset[1] + "," + sensoroffset[2] + "," + sensoroffset[3] + "," + irheading[0] + "," + irheading[1] + "," + irheading[2] + "," + irheading[3] + "," + messageDeadline + "}\n";
    }

    
    /**
     * Generates an update message.
     * @return String.
     */
    String generateUpdate() {
        int xPos = (int)Math.round(estimatedPose.getPosition().getXValue());
        int yPos = (int)Math.round(estimatedPose.getPosition().getYValue());
        int robotHeading = (int)Math.round(estimatedPose.getHeading().getValue());
        int towerHeading = (int)Math.round(towerAngle.getValue());
        int s1 = (int)Math.round(lastIrMeasurement[0]);
        int s2 = (int)Math.round(lastIrMeasurement[1]);
        int s3 = (int)Math.round(lastIrMeasurement[2]);
        int s4 = (int)Math.round(lastIrMeasurement[3]);
        return "{U," + xPos + "," + yPos + "," + robotHeading + "," + towerHeading + "," + s1 + "," + s2 + "," + s3 + "," + s4 + "}\n";
    }
}
