/*
 * This code is written as a part of a Master Thesis
 * the spring of 2016.
 *
 * Eirik Thon(Master 2016 @ NTNU)
 */
package no.ntnu.et.simulator;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import javax.swing.JFrame;
import no.ntnu.tem.communication.Inbox;

/**
 * This is the main class of the simulator. It contains the simulator's API, and
 * also the control system for the robots.
 *
 * @author Eirik Thon
 */
public class Simulator {
    private final SimWorld world;
    private SimulatorGUI gui;
    private boolean estimateNoiseEnabled;
    private boolean sensorNoiseEnabled;
    HashMap<Integer, RobotHandler> robotHandlers;
    private double simulationSpeed;
    private Inbox inbox;
    private int mode;
    private HashMap<Integer, String> idNameMapping;

    /**
     * Constructor.
     * 
     * @param inbox The inbox of the application where messages from the robots
     * should be stored.
     */
    public Simulator(Inbox inbox) {
        simulationSpeed = 1;
        world = new SimWorld();
        File mapFolder = new File("maps");
        File[] allMaps = mapFolder.listFiles();
        if(allMaps.length == 0){
            System.out.println("No map files found");
        }
        String mapPath = allMaps[0].getAbsolutePath();
        world.initMap(mapPath);
        robotHandlers = new HashMap<Integer, RobotHandler>();
        estimateNoiseEnabled = true;
        sensorNoiseEnabled = true;
        this.mode = mode;
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                initializeGUI();
            }
        });
        this.inbox = inbox;
    }

    /**
     * Changes the simulation speed. In practice this changes how fast the robot
     * handlers goes through an iteration of the robot-behavior loop. This makes
     * the robots do everything much faster, which in practice increases the
     * simulation speed.
     * @param newSpeed the new speed of the simulator.
     */
    void setSimulationSpeed(double newSpeed) {
        simulationSpeed = (double) newSpeed / 100;
    }

    /**
     * Makes the GUI of the simulator visible to the user.
     */
    public void openGui() {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                gui.setVisible(true);
            }
        });
    }

    /**
     * Hides the GUI of the simulator from the user.
     */
    public void closeGui() {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                gui.setVisible(false);
            }
        });
    }

    /**
     * Stops the simulator completely.
     */
    public void stop() {
        // Uses the closing routine defined in initializeGUI
        for (HashMap.Entry<Integer, RobotHandler> entry : robotHandlers.entrySet()) {
            entry.getValue().interrupt();
        }
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                gui.dispose();
            }
        });
    }

    /**
     * This function simulates how the application connects to the real robots.
     * The simulator creates a new thread with a robot handler for controlling
     * the specified robot. The specified robot starts its normal operation
     * after this function has been called.
     * @param id The ID of the robot that should be started.
     */
    public void connectToRobot(int id) {
        if (!robotHandlers.containsKey(id)) {
            SimRobot robot = world.getRobot(id);
            RobotHandler robotHandler = this.new RobotHandler(robot);
            robotHandler.setName("Robot " + Integer.toString(id));
            robotHandlers.put(id, robotHandler);
            robotHandlers.get(id).start();
        }
    }

    /**
     * Stops the robot behavior completely. However the SimRobot object still
     * exists in the simulator and can be reconnected to again.
     * @param id This variable specifies which robot that should be
     * disconnected.
     */
    public void disconnectRobot(int id) {
        if (robotHandlers.containsKey(id)) {
            robotHandlers.get(id).interrupt();
            robotHandlers.remove(id);
        }
    }

    /**
     * Returns the names and IDs of all the SimRobot objects in the simulator.
     * @return ArrayList of Strings arrays. The first part of the string array
     * is the ID, the second part is the name.
     */
    public ArrayList<String[]> listAvailableRobots() {
        ArrayList<String[]> availableRobots = new ArrayList<String[]>();
        ArrayList<Integer> robotIDs = world.getRobotIDs();
        for (int i = 0; i < robotIDs.size(); i++) {
            String name = world.getRobot(robotIDs.get(i)).getName();
            String[] robotId = {"" + robotIDs.get(i), name};
            availableRobots.add(robotId);
        }
        return availableRobots;
    }

    /**
     * Turns estimation error on/off
     */
    void flipEstimateNoiseEnabled() {
        estimateNoiseEnabled = !estimateNoiseEnabled;
    }

    /**
     * Turns sensor error on/off
     */
    void flipSensorNoiseEnabled() {
        sensorNoiseEnabled = !sensorNoiseEnabled;
    }

    /**
     * Gives a new command to the robot with the specified ID.
     * @param id The ID of the robot.
     * @param rotation The amount the robot should rotate.
     * @param distance The amount the robot should move forward.
     */
    public void setRobotCommand(int id, double rotation, double distance) {
        world.getRobot(id).setTarget(rotation, distance);
    }

    /**
     * Pauses the operation of the specified robot.
     * @param robotId The Id of the robot that should be paused.
     */
    public void pauseRobot(int robotId) {
        robotHandlers.get(robotId).pause();
    }

    /**
     * Resumes the operation of the specified robot.
     * @param robotId The ID of the robot that should resume.
     */
    public void unpauseRobot(int robotId) {
        robotHandlers.get(robotId).unpause();
    }

    /**
     * Initializes the graphical user interface
     */
    private void initializeGUI() {
        gui = new SimulatorGUI(this, world);
        gui.setTitle("Simulator");
        gui.setVisible(false);
        gui.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }

    /**
     * Controller object for a robot. This class contains a SimRobot object and 
     * is responsible for making that robot run.
     */
    private class RobotHandler extends Thread {
        final private SimRobot myRobot;
        final private String myName;
        final private int myID;
        private double estimateNoise;
        private double sensorNoise;
        private final Random noiseGenerator;
        private boolean paused;

        /**
         * Constructor
         * @param robot SimRobot
         */
        public RobotHandler(SimRobot robot) {
            myRobot = robot;
            myName = robot.getName();
            myID = robot.getId();
            noiseGenerator = new Random();
            paused = false;
        }

        /**
         * Pauses the robot handler
         */
        void pause() {
            paused = true;
        }
        
        /**
         * Resumes the robot handler
         */
        void unpause() {
            paused = false;
        }

        /**
         * A continuous loop that calls methods of the robot in a specific way
         * in order to generate the robot behavior.
         */
        @Override
        public void run() {
            int counter = 0;
            String content = myRobot.generateHandshake();
            String dongleID = "[" + myID + "]:" + myName + ":";
            String handshake = dongleID + content;
            inbox.putMessage(handshake);
            while (true) {
                // Wait between each loop
                try {
                    Thread.sleep((int) (10 / simulationSpeed));
                } catch (InterruptedException e) {
                    break;
                }
                if (paused) {
                    continue;
                }

                // Move robot
                if (estimateNoiseEnabled) {
                    estimateNoise = noiseGenerator.nextGaussian() * 0.1;
                } else {
                    estimateNoise = 0;
                }
                if (myRobot.moveRobot(estimateNoise) == true) {
                    String updateMsg = "{S,IDL}\n";
                    String dongleSim = "[" + myID + "]:" + myName + ":";
                    inbox.putMessage(dongleSim + updateMsg);
                }
                myRobot.turnTower();
                
                // Measure
                if (counter > 19) { // Every 200 ms ( Counter is increased every 10 ms )
                    if (sensorNoiseEnabled) {
                        sensorNoise = noiseGenerator.nextGaussian() * 2;
                    } else {
                        sensorNoise = 0;
                    }
                    myRobot.measureIR(sensorNoise);
                    String updateMsg = myRobot.generateUpdate();
                    String dongleSim = "[" + myID + "]:" + myName + ":";
                    inbox.putMessage(dongleSim + updateMsg);
                    counter = 0;
                }
                counter++;
            }
        }
    }
}
